jQuery(document).ready(function ($) {
  let pikcupPoints = [];

  // SUBMIT CP
  $("#submit_btn_javit").click(function () {
    //CLEAR
    $("#pointStore").remove();
    $("#dir-javit").remove();
    $("#hours-javit").remove();
    $("#javit_error").hide();
    $("#javit_pickup_name").val();
    $("#javit_pickup_id").val();
    $("#javit_pickup_code").val();

    let cp = $("#javit_cp").val();
    let storeCode = $("#javit_store_code").val();
    let global_url = $("#javit_global_url").val();

    if (cp && storeCode && global_url) {
      $.ajax({
        url: global_url,
        type: "POST",
        dataType: "json",
        data: JSON.stringify({
          destination: {
            postal_code: cp,
            storeCode: storeCode,
          },
        }),
        contentType: "application/json; charset=utf-8",
        headers: {
          traza: "javit-wp",
          aplicacionOrigen: "swagger",
        },
        beforeSend: function () {
          $("#loaderJavit").show();
        },
        success: function (res) {
          $("#loaderJavit").hide();
          if (res?.rates) {
            pikcupPoints = res.rates;
            let select = `<select id="pointStore" name="pointlist" class="javit-select">
              <option value="" id="select-option-javit">Seleccione un punto de entrega</option>
              </select>`;

            if ($("#pointStore").length === 0) {
              $("#submit_btn_javit").after(select);
            }

            res.rates.forEach((point, i) => {
              $("#pointStore").append(
                `<option value="${point.id}" id="select-option-javit">${point.name}</option>`
              );
            });
          }
        },
        error: function (error) {
          console.error(error?.responseJSON);
          $("#loaderJavit").hide();
          $("#javit_error").text("No se encontraron puntos de entrega.");
          $("#javit_error").show();
          return false;
        },
      });
    } else {
      $("#javit_error").text("El campo C.P. es requerido.");
      $("#javit_error").show();
    }
  }); //end submit

  //SELECTED OPTION
  $(document.body).on("change", "select#pointStore", function () {
    let selectedItem = $("#pointStore :selected").val();
    if (selectedItem && pikcupPoints.length > 0) {
      let pikcupPointsFiltered = pikcupPoints.filter(
        (s) => s.id === parseInt(selectedItem)
      )[0];
      if (pikcupPointsFiltered) {
        $("#javit_pickup_name").val(pikcupPointsFiltered.name);
        $("#javit_pickup_code").val(pikcupPointsFiltered.pickup_code);
        $("#javit_pickup_id").val(selectedItem);
        let dirText = `Farmacia ${pikcupPointsFiltered.name}, ${pikcupPointsFiltered.address.address} ${pikcupPointsFiltered.address.number}, ${pikcupPointsFiltered.address.province}, ${pikcupPointsFiltered.address.locality}, ${pikcupPointsFiltered.address.city}.`;

        let formatHours = pikcupPointsFiltered.hours.map((hour) => {
          let customHours = `${hour.start.slice(0, 2) + ":" + hour.start.slice(2)
            } - ${hour.end.slice(0, 2) + ":" + hour.end.slice(2)}`;
          switch (hour.day) {
            case 0:
              return `domingo ${customHours}`;
            case 1:
              return `lunes ${customHours}`;
            case 2:
              return `martes ${customHours}`;
            case 3:
              return `miercoles ${customHours}`;
            case 4:
              return `jueves ${customHours}`;
            case 5:
              return `viernes ${customHours}`;
            case 6:
              return `sabado ${customHours}`;
          }
        });

        let hoursText = `Horarios: ${formatHours.join(", ")}`;
        let txt1 = $("#dir-javit").text() ? $("#dir-javit").text(dirText) : `<p id='dir-javit'>${dirText}</p>`;
        let txt2 = $("#hours-javit").text() ? $("#hours-javit").text(hoursText) : `<p id='hours-javit'>${hoursText}</p>`;
        $("#pointStore").after(txt1, txt2);
      }
    } else {
      $("#javit_pickup_name").val("");
      $("#javit_pickup_code").val("");
      $("#javit_pickup_id").val("");
      $("#dir-javit").remove();
      $("#hours-javit").remove();
    }
  });
});
