<?php

/**
 * Add select
 **/

// add styles
add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts()
{
   if (is_checkout() || is_cart()) {
      wp_register_style('javit-styles', plugins_url() . '/javit/assets/css/styles.css');
      wp_enqueue_style('javit-styles');
   }
}

// add select for javit 
function current_shipping($method, $index)
{
   $chosen  = WC()->session->get('chosen_shipping_methods')[0]; // The chosen methods
   $javit_store_code = get_option('javit_store_code');

   if ($method->get_id() == 'javit_shipping' && $chosen === 'javit_shipping' && is_checkout()) {
      require_once WP_PLUGIN_DIR . '/javit/templates/select-javit.php';
   }
}
add_action('woocommerce_after_shipping_rate', 'current_shipping', 20, 2);

// Pickup store Validation required selected
add_action('woocommerce_checkout_process', 'validate_pickup_store');
function validate_pickup_store()
{
   $chosen_shipping_rate_id = WC()->session->get('chosen_shipping_methods')[0];
   // wc_add_notice( __( 'add '.json_encode($_POST)), 'error' );
   if (
      false !== strpos($chosen_shipping_rate_id, 'javit_shipping')
      && isset($_POST['javit_pickup_id']) && empty($_POST['javit_pickup_id'])
   ) {
      wc_add_notice(__('Debe seleccionar un punto de entrega para los medios de envío Javit.', 'javit_shipping'), 'error');
   }
}

// Save chosen pickup store as order meta
add_action('woocommerce_checkout_create_order', 'save_pickup_stores_to_order', 10, 2);
function save_pickup_stores_to_order($order, $data)
{
   if (isset($_POST['javit_pickup_id']) && !empty($_POST['javit_pickup_id'])) {
      $order->update_meta_data('javit_pickup_id', esc_attr($_POST['javit_pickup_id']));
      $order->update_meta_data('javit_pickup_name', esc_attr($_POST['javit_pickup_name']));
      $order->update_meta_data('javit_pickup_code', esc_attr($_POST['javit_pickup_code']));
   }
}

// Save chosen pickup store as order shipping item meta, crea dentro de shipping_lines el medio de envio
add_action('woocommerce_checkout_create_order_shipping_item', 'save_pickup_stores_to_order_item_shipping', 10, 4);
function save_pickup_stores_to_order_item_shipping($item, $package_key, $package, $order)
{
   if (isset($_POST['javit_pickup_id']) && !empty($_POST['javit_pickup_id'])) {
      $item->update_meta_data('_javit_pickup_id', esc_attr($_POST['javit_pickup_id']));
      $item->update_meta_data('_javit_pickup_name', esc_attr($_POST['javit_pickup_name']));
      $item->update_meta_data('_javit_pickup_code', esc_attr($_POST['javit_pickup_code']));
   }
}

// Admin order: Change store order shipping item displayed meta key label to something readable
add_filter('woocommerce_order_item_display_meta_key', 'filter_order_item_displayed_meta_key', 20, 3);
function filter_order_item_displayed_meta_key($displayed_key, $meta, $item)
{
   // Change displayed meta key label for specific order item meta key
   if ($item->get_type() === 'shipping' && $meta->key === '_javit_pickup_id') {
      $displayed_key = __("Punto id", "javit_shipping");
   }
   if ($item->get_type() === 'shipping' && $meta->key === '_javit_pickup_name') {
      $displayed_key = __("Punto nombre", "javit_shipping");
   }
   if ($item->get_type() === 'shipping' && $meta->key === '_javit_pickup_code') {
      $displayed_key = __("Punto codigo", "javit_shipping");
   }
   return $displayed_key;
}

// Customer: Display Store below shipping method on orders and email notifications
add_filter('woocommerce_get_order_item_totals', 'display_pickup_store_on_order_item_totals', 10, 3);
function display_pickup_store_on_order_item_totals($total_rows, $order, $tax_display)
{
   $chosen_store   = $order->get_meta('javit_pickup_name'); // Get pickup store
   $new_total_rows = array(); // Initializing

   if (empty($chosen_store))
      return $total_rows; // Exit

   // Loop through total rows
   foreach ($total_rows as $key => $value) {
      if ('shipping' == $key) {
         $new_total_rows['javit_pickup_name'] = array(
            'label' => __("Punto de entrega Javit", "javit_shipping") . ':',
            'value' => $chosen_store,
         );
      } else {
         $new_total_rows[$key] = $value;
      }
   }
   return $new_total_rows;
}

function set_global_var()
{
   global $javit_url;
   $javit_url = 'https://api.javit.com.ar/api/puntosDeEntregaPublico/1/GetRates';
}
add_action('init', 'set_global_var');
