<?php

/**
 * Config in admin menu
 **/

add_action('admin_menu', 'javit_add_menu_page');

function javit_add_menu_page()
{
    $icon_base64 = 'PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA3Mi45IDEwMCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDljZGU7fTwvc3R5bGU+PC9kZWZzPjx0aXRsZT5SZWN1cnNvIDE8L3RpdGxlPjxnIGlkPSJDYXBhXzIiIGRhdGEtbmFtZT0iQ2FwYSAyIj48ZyBpZD0iQ2FwYV8xLTIiIGRhdGEtbmFtZT0iQ2FwYSAxIj48ZyBpZD0iQ2FwYV8yLTIiIGRhdGEtbmFtZT0iQ2FwYSAyIj48ZyBpZD0iQ2FwYV8xLTItMiIgZGF0YS1uYW1lPSJDYXBhIDEtMiI+PGcgaWQ9IkNhcGFfMi0yLTIiIGRhdGEtbmFtZT0iQ2FwYSAyLTIiPjxnIGlkPSJDYXBhXzEtMi0yLTIiIGRhdGEtbmFtZT0iQ2FwYSAxLTItMiI+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNLjksMEgxOS44YS44LjgsMCwwLDEsLjguNmwxNSwzOC43YS44LjgsMCwwLDAsMS42LDBMNTEuOS42YS44LjgsMCwwLDEsLjgtLjZINzIuMWEuOC44LDAsMCwxLC44LjhoMHYuM0w2OC45LDExLjksNDEuMyw4OS40YzcuNS42LDEzLDIuNiwxMyw1LjJTNDYuMiwxMDAsMzYuNSwxMDBzLTE3LjgtMi40LTE3LjgtNS41LDUuNS00LjYsMTIuOS01LjJMLjMsMi4xLjEsMS4yQS44LjgsMCwwLDEsLjQuMUguOVoiLz48L2c+PC9nPjwvZz48L2c+PC9nPjwvZz48L3N2Zz4=';
    $icon_data_uri = 'data:image/svg+xml;base64,' . $icon_base64;

    add_menu_page(
        'Javit',                        // Page title
        'Javit Ajustes',                // Menu title
        'manage_options',               // Capability
        'javit-settings',               // Menu slug
        'javit_display_settings_page',  // Callable
        $icon_data_uri
    );
    // position in menu
}

function javit_display_settings_page()
{
    require_once WP_PLUGIN_DIR . '/javit/templates/settings-page.php';
}

add_action('admin_init', 'javit_register_settings');

function javit_register_settings()
{

    register_setting(
        'javit_settings_group',     // Settings group name
        'javit_store_code',         // Option name
        'sanitize_text_field'       // Arguments or sanitize funcion
    );
    register_setting('javit_settings_group', 'javit_store_code', 'field_validation');
}

// validate code store
function field_validation($value)
{
    if (validStrLen($value, 5, 10)) {
        $value = get_option('javit_store_code');
        add_settings_error('my_option_notice', 'javit_store_code', 'El código ingresado no es válido');
    }
    return $value;
}

function validStrLen($str, $min, $max){
    $len = strlen($str);
    if($len < $min || $len > $max){
        return true;
    }
    return false;
}