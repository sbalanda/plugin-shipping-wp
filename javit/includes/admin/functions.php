<?php

/*
 * Create Shipping method
 * Check if WooCommerce is active
 */
//ADD ACCION SHIPPING METHOD
add_action('woocommerce_shipping_init', 'javit_shipping_init');

// FUNCTION
function javit_shipping_init()
{
    if (!class_exists('WC_JAVIT_SHIPPING')) {

        class WC_JAVIT_SHIPPING extends WC_Shipping_Method
        {

            public function __construct()
            {
                $this->id = 'javit_shipping'; // Id for your shipping method. Should be uunique.
                $this->method_title = __('Puntos de entrega Javit');  // Title shown in admin
                $this->method_description = __('Medio de envío Javit'); // Description shown in admin
                $this->enabled = "yes"; // This can be added as an setting but for this example its forced enabled
                $this->title = "Puntos de entrega Javit"; // This can be added as an setting but for this example its forced.
                $this->init();
            }

            public function init()
            {
                // Load the settings API
                $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
                $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

                // Save settings in admin if you have any defined
                add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
            }

            /**
             * calculate_shipping function.
             * @param array $package (default: array())
             */
            public function calculate_shipping($package = array())
            {

                $rate = array(
                    'label' => $this->title,
                    'cost' => '00.00',
                    'calc_tax' => 'per_item'
                );

                // Register the rate
                $this->add_rate($rate);
            }
        }
    }
}

add_filter('woocommerce_shipping_methods', 'add_javit_method');
function add_javit_method($methods)
{
    $methods['javit_shipping'] = 'WC_JAVIT_SHIPPING';
    return $methods;
}


// crear nuevo status
function register_custom_order()
{
    register_post_status('wc-procesar-javit', array(
        'label' => __('Procesar Javit', 'woocommerce'),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('Procesar Javit <span class="count">(%s)</span>', 'Procesar Javit <span class="count">(%s)</span>')
    ));
    register_post_status('wc-procesado-javit', array(
        'label' => __('Procesado Javit', 'woocommerce'),
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('Procesado Javit <span class="count">(%s)</span>', 'Procesado Javit <span class="count">(%s)</span>')
    ));
}
add_action('init', 'register_custom_order');


function add_new_status($wc_statuses_arr)
{

    $new_statuses_arr = array();

    // add new order status after processing
    foreach ($wc_statuses_arr as $id => $label) {
        $new_statuses_arr[$id] = $label;

        if ('wc-processing' === $id) { // after "Completed" status
            $new_statuses_arr['wc-procesar-javit'] = 'Procesar Javit';
            $new_statuses_arr['wc-procesado-javit'] = 'Procesado Javit';
        }
    }

    return $new_statuses_arr;
}
add_filter('wc_order_statuses', 'add_new_status');

// cambio de orden automatico
add_action('woocommerce_order_status_changed', 'woocommerce_auto_processing_orders');
function woocommerce_auto_processing_orders($order_id)
{
    if (!$order_id) return;

    $order = wc_get_order($order_id);
    if (!$order) return;

    $data = $order->get_data();
    if (!$data) return;

    // si el estado actual es procesing y si existe el estado javit
    if ($order->has_status('processing') && in_array('wc-procesar-javit', array_keys(wc_get_order_statuses()))) {
        // si existe el medio de envio
        foreach ($data["shipping_lines"] as $s) {
            if ($s["method_id"] == 'javit_shipping') {
                $order->update_status('wc-procesar-javit');
            }
        }
    }
}
