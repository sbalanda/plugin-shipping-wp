<h1>Javit</h1>
<h3>Somos un servicio de envíos que une a e-commerce con clientes de todo el país.</h3>
<hr>
<?php settings_errors();  ?>
<p>Ingrese el código de tienda proporcionado por Javit.</p>
<form method="post" action="options.php">

    <?php settings_fields('javit_settings_group'); ?>

    <table class="form-table">
        <tr valign="top">
            <th scope="row" class="javit_label_form">Código de tienda:</th>
            <td>
                <input type="text" name="javit_store_code" class="javit_store_code" placeholder="Código" value="<?php echo esc_attr(get_option('javit_store_code')); ?>" required/>
            </td>
        </tr>
    </table>

    <?php submit_button(); ?>
</form>