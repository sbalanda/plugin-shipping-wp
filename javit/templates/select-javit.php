<div>
    <form name="javit_pickup" action="" class="javit-form-points">
        <p for="javit_label" id="javit_label" class="javit-label">Ingresá tu código postal para ver las mejores opciones de retiro.</p>
        <input type="text" name="javit_cp" id="javit_cp" class="text-input" placeholder="Ej 1602" required />
        <p class="javit-error" for="javit_error" id="javit_error" style="display:none"></p>

        <input type="button" name="submit" class="button-javit" id="submit_btn_javit" value="Enviar" />
    </form>

    <?php echo '<input type="hidden" value="' . $javit_store_code . '" name="javit_store_code" id ="javit_store_code"/>' ?>
    <?php echo '<input type="hidden" name="javit_pickup_name" id="javit_pickup_name"/>' ?>
    <?php echo '<input type="hidden" name="javit_pickup_id" id="javit_pickup_id"/>' ?>
    <?php echo '<input type="hidden" name="javit_pickup_code" id="javit_pickup_code"/>' ?>
    <?php 
        global $javit_url;
        echo '<input type="hidden" value="' . $javit_url . '" name="javit_global_url" id ="javit_global_url"/>'
    ?>
    <div id="loaderJavit" style="display:none">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
</div>

<!-- CALLING API -->
<script>
    <?php require_once(WP_PLUGIN_DIR.'/javit/assets/js/select-javit.js');?>
</script>