<?php

/**
 * Plugin Name: Javit
 * Plugin URI: https://www.globons.com/
 * Description: Medio de envío Javit.
 * Version: 0.0.1
 * Author: Globons
 * Author URI: https://www.globons.com/
 * License: GPLv2
 * Text Domain: Javit
 */

// evitar que accedan directamente a tu plugin
if (!defined('WPINC')) {
   die;
}

/**
 * Check if javit is active
**/
if (in_array('javit/javit.php', apply_filters('active_plugins', get_option('active_plugins'))) && in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
   include plugin_dir_path(__FILE__) . "/includes/public/functions.php";
   include plugin_dir_path(__FILE__) . "/includes/admin/functions.php";
   include plugin_dir_path(__FILE__) . '/includes/admin/configurations.php';
}
