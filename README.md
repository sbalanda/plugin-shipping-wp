# Instalacion de wordpress en ambiente local

- instalar https://laragon.org/download/ seguir los pasos
- iniciar laragon
- ir a la opcion de creacion rapida de laragon seleccionar wordpress seguir pasos
- instalar woocommerce plugin
- habilitar una zona de envio y pago

# INSTALACION
- que carpeta?
- Comprimir proyecto en un zip
- completar codigo
- Desde el admin de la tienda ir a plugins, agregar nuevo, subir plugin, instalar, activar y agregar el codigo de tienda en en la Javit Ajustes. Al momento en develop los codigos de tienda pueden ser NESTLE o HEROKU.

# INFO

- javit.php - inicio se llaman a todos los archivos del plugin
- admin/functions.php - se crea el medio de envíos, estados, cambio de orden automático
- public/functions.php - se agregan estilos, select que se muestra en el carrito y url global para los distintos - ambientes en método set_global_var(), se guardan datos del punto de envío dentro del pedido de wordpress
- admin/configurations - settings menu admin
- templates/select-javit.php - formulario en el carrito, donde se ingresa el código postal
- templates/settings-page - page settings Javit
- assets - styles y select

# DOCUMENTS WP

- https://stackoverflow.com/questions/66716106/custom-pickup-shipping-method-with-a-dropdown-of-stores-in-woocommerce
- https://code.tutsplus.com/es/tutorials/create-a-custom-shipping-method-for-woocommerce--cms-26098
- https://www.youtube.com/watch?v=k8FACBC7VtY
- https://www.tychesoftwares.com/creating-a-new-shipping-method-and-exploring-the-shipping-method-api-of-woocommerce/
- https://webdesign.tutsplus.com/es/articles/how-to-incorporate-external-apis-in-your-wordpress-theme-or-plugin--cms-33542
- https://programmierfrage.com/items/extra-carrier-field-for-shipping-methods-in-woocommerce-cart-and-checkout
- https://wooenvio.es/desarrollo-plugin-wordpress-pagina-configuracion/
- https://codigonaranja.com/pagina-de-ajustes-para-plugin-de-wordpress
- https://base64.guru/converter/encode/image/ico


